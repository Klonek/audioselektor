# AudioSelektor
<<<<<<< HEAD
![alt text](shematics/photo.jpg)
## Description
Audio selector and power switch for my custom tube amp. Code write with Platformio. 
## Sheme
![alt text](shematics/sheme.svg)
## Selector PCB
![alt text](shematics/selector_board_1.png)
![alt text](shematics/selector_board_2.png)
## Power relays PCB
![alt text](shematics/power_relays_board_1.png)
![alt text](shematics/power_relays_board_2.png)
## Rotatory switch PCB
![alt text](shematics/switch_board_1.png)
![alt text](shematics/switch_board_2.png)
=======
![photo](shematics/photo.jpg)
## Description
Audio selector and power switch for my custom tube amp. Code write using Platformio framework. It has 5 asymetrical channels, own power supply and it drive by WaveShre RP2040-ZERO. 
## Connections
- T_6.3V - power connection 6.3V-9V AC
- SW_POWER - output for switching amp power
- U1-U5 - 5 fully isolated asymetrical audio inputs L+ L- R- R+
- U6- audio output L+ L- R- R+
## Build and deploy
Install PlatformIO extension and use it.
Or install PlatformIO CLI and run
```
platformio run --target upload --environment waveshare_rp2040_zero
```
## Sheme
![shema](shematics/sheme.svg)
## Selector PCB
![pcb](shematics/selector_board_1.png)
![pcb](shematics/selector_board_2.png)
## Power relays PCB
![pcb](shematics/power_relays_board_1.png)
![pcb](shematics/power_relays_board_2.png)
## Rotatory switch PCB
![pcb](shematics/switch_board_1.png)
![pcb](shematics/switch_board_2.png)
>>>>>>> 561bdaa (Added readme)
## License
The MIT License

Copyright 2023 Piotr Kasperski

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

