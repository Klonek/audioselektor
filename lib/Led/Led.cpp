#include "Led.h"

Led::Led()
{
    this->leds = new CRGB();
    FastLED.addLeds<WS2812, LED_PIN, GRB>(this->leds, 1);
}

Led::~Led()
{
}

void Led::set(uint8_t red, uint8_t green, uint8_t blue)
{
    CRGB color;
    color.red = red;
    color.green = green;
    color.blue = blue;
    this->leds[0] = color;
    FastLED.show();
}
