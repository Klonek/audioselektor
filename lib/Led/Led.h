#ifndef LED_H
#define LED_H
#include <Arduino.h>
#include <FastLED.h>

#ifndef LED_PIN
#define LED_PIN 16
#endif // !LED_PIN

#pragma once

class Led
{
public:
    Led();
    ~Led();
    void set(uint8_t red, uint8_t green, uint8_t blue);

private:
    CRGB *leds;
};

#endif