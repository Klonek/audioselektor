#ifndef RELAY_H
#define RELAY_H
#include <Arduino.h>
#define OPERATING_TIME 50
#pragma once

class Relay
{
public:
    Relay(int coil_A, int coil_B);
    void set();
    void reset();
    void begin();

private:
    int setPin, resetPin;
};

#endif