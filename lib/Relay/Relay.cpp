#include "Relay.h"

Relay::Relay(int setPin, int resetPin)
{
  this->resetPin = resetPin;
  this->setPin = setPin;
  this->begin();
}

void Relay::set()
{
  Serial.print("Set relay on pin: ");
  Serial.println(this->setPin);
  Serial.println("Set relay");
  digitalWrite(this->setPin, LOW);
  delay(OPERATING_TIME);
  digitalWrite(this->setPin, HIGH);
  delay(OPERATING_TIME);
};
void Relay::reset()
{
  Serial.print("Reset relay on pin: ");
  Serial.println(this->setPin);
  digitalWrite(this->resetPin, LOW);
  delay(OPERATING_TIME);
  digitalWrite(this->resetPin, HIGH);
  delay(OPERATING_TIME);
};
void Relay::begin()
{ // Setting realy pins
  pinMode(this->resetPin, OUTPUT);
  pinMode(this->setPin, OUTPUT);
  digitalWrite(this->resetPin, HIGH);
  digitalWrite(this->setPin, HIGH);
};