#include "Switch.h"

Switch::Switch(short dataPin, SWADDR adressPins, short positions)
{
    this->dataPin = dataPin;
    this->adressPins = adressPins;
    this->positions = positions;
    this->setPinouts();
}

Switch::~Switch()
{
}

short Switch::getPosition()
{
    for (short i = 0; i < this->positions; i++)
    { // change switch address
        this->setAddress({((i / 2) / 2) % 2, (i / 2) % 2, i % 2});
        // return enabled switch position
        if (this->getOutput() == 1)
            return i + 1;
    }
    return 0;
}

void Switch::setPinouts()
{ // Setting switch pins
    pinMode(this->adressPins.A0, OUTPUT);
    pinMode(this->adressPins.A1, OUTPUT);
    pinMode(this->adressPins.A2, OUTPUT);
    pinMode(this->dataPin, INPUT);
}

void Switch::setAddress(SWADDR address)
{ // Setting switch multiplexer address
    digitalWrite(this->adressPins.A0, address.A0);
    digitalWrite(this->adressPins.A1, address.A1);
    digitalWrite(this->adressPins.A2, address.A2);
}

short Switch::getOutput()
{ // read swich value at current address
    return digitalRead(this->dataPin);
}
