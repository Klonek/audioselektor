#ifndef SWITCH_H
#define SWITCH_H
#include <Arduino.h>

#pragma once
struct SWADDR
{
  short A0;
  short A1;
  short A2;
};

class Switch
{
public:
  Switch(short dataPin, SWADDR adressPins, short positions);
  ~Switch();
  short getPosition();

private:
  short dataPin;
  SWADDR adressPins;
  short positions;
  void setPinouts();
  void setAddress(SWADDR address);
  short getOutput();
};

#endif