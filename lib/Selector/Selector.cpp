#include "Selector.h"

Selector::Selector(
    Switch &sw, std::vector<Relay *> &relays,
    Led &led,
    short enablePin,
    short channels)
{
    this->sw = &sw;
    this->relays = &relays;
    this->led = &led;
    this->enablePin = enablePin;
    this->channels = channels;
    this->enableChannel = 0;
    this->resetAll();
}

Selector::~Selector()
{
}

void Selector::update()
{ // Getting current swich position;
    this->switchPosition = this->sw->getPosition();
    // Checking if position didn't exceed channel limit
    if (this->switchPosition > this->channels)
        this->switchPosition = this->channels;
    // turn off relays if switch is on off position
    if (this->switchPosition == 0)
    {
        if (this->enableChannel == 0)
            return;
        Serial.println("Position chenge to 0 reseting all relays");
        this->enableChannel = 0;
        // this->setLed(this->enableChannel, 0);
        this->resetAll();
        return;
    }
    // Setting relay
    if (this->switchPosition != this->enableChannel)
    {
        Serial.print("Changin channel to: ");
        Serial.println(this->switchPosition);
        this->enableChannel = this->switchPosition;
        // Reset all switches
        this->resetAll();
        // Set proper channel
        this->setChannel(this->enableChannel);
        // Set RGB
        this->setLed(this->enableChannel, 50);
        return;
    }
}

void Selector::resetAll()
{
    // digitalWrite(this->enablePin, HIGH);
    for (short i = 0; i < this->channels; i++)
    { // reset relay
        this->relays->at(i)->reset();
    }
    // digitalWrite(this->enablePin, LOW);
}

void Selector::setChannel(short channel)
{
    // digitalWrite(this->enablePin, HIGH);
    // Set realy
    this->relays->at(this->enableChannel - 1)->set();
    // digitalWrite(this->enablePin, LOW);
}

void Selector::setLed(short channel, short brightness)
{ // Set RGB color
    this->led->set((((channel / 2) / 2) % 2) * brightness,
                   ((channel / 2) % 2) * brightness,
                   (channel % 2) * brightness);
}
