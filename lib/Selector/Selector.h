#ifndef SELECTOR_H
#define SELECTOR_H
#include <Led.h>
#include <Switch.h>
#include <Relay.h>
#include <Arduino.h>
#pragma once

class Selector
{
public:
    Selector(
        Switch &sw, std::vector<Relay *> &relays,
        Led &led,
        short enablePin,
        short channels);
    ~Selector();
    void update();

private:
    Led *led;
    Switch *sw;
    std::vector<Relay *> *relays;
    short enablePin;
    short enableChannel;
    bool hasChanged;
    short switchPosition;
    short channels;
    void resetAll();
    void setChannel(short channel);
    void setLed(short channel, short brightness);
};

#endif