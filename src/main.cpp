#include <Arduino.h>
#include <Selector.h>
// RGB PINOUT
#define LED_PIN 16
// SWITCH PINOUT
#define SWITCH_PIN 0
#define SWITCH_A0 11
#define SWITCH_A1 12
#define SWITCH_A2 13
#define SWICH_POSITIONS 5
// RELAYS PINOUT
#define CH_1_PIN 8
#define CH_2_PIN 7
#define CH_3_PIN 6
#define CH_4_PIN 5
#define CH_5_PIN 4
#define RESET_PIN 3
// SELECTOR PINOUT
#define ENABLE_PIN 2
#define CHANNELS 4

// Hardware definitions
Led *led;
Switch *sw;
std::vector<Relay *> *relays;
Selector *selector;
Relay relay(CH_5_PIN, RESET_PIN);
void setup()
{
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, HIGH);
  // Starting serial;
  Serial.begin(115200);
  while (!Serial && millis() < 5000)
    ;
  Serial.println("AudioSelector");
  // Setting hardware
  led = new Led();
  sw = new Switch(
      SWITCH_PIN,
      {SWITCH_A0, SWITCH_A1, SWITCH_A2},
      SWICH_POSITIONS);
  relays = new std::vector<Relay *>;
  relays->push_back(new Relay(CH_1_PIN, RESET_PIN));
  relays->push_back(new Relay(CH_2_PIN, RESET_PIN));
  relays->push_back(new Relay(CH_3_PIN, RESET_PIN));
  relays->push_back(new Relay(CH_4_PIN, RESET_PIN));
  // relays->push_back(new Relay(CH_5_PIN, RESET_PIN)); //ONLY 4 WORKING RELAYS
  selector = new Selector(
      *sw,
      *relays,
      *led,
      ENABLE_PIN,
      CHANNELS);
}

void loop()
{ // program
  selector->update();
}
